package com.alticast.charter.telemetry;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.alticast.charter.telemetry.collector.DataCollector;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements View.OnKeyListener {
    private static TextView console;
    private static ListView listView;
    private static ScrollView consoleBg;
    public static com.alticast.charter.telemetry.MainActivity activity;
    PowerManager powerManager;
    public static String thermalStatus = "NONE";

    private static String[] fNames = {
        "getTime"
            , "getVersion"
            , "getDiagInfo"
            , "getCpuUsage"
            , "getStorageUsage"
            , "getMemoryUsage"
            , "getNetworkInterface"
            , "getSetupValue"
            , "getProcesses"
            , "reboot"
            , "factoryReset"
            , "showDiagScreen"
            , "killProcess"
            , "getThermalStatus"
            , "uploadFile"
            , "deleteFile"
            , "setLogPolicy"
            , "getLogPolicy"
            , "uploadLog"
            , "getRunningAppInfo"
            , "getInstalledAppInfo"
            , "runApp"
    };

    private static List<String> functions = new ArrayList<String>();

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            // REBOOT
            case 101: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setConsoleLog("Permission Granted", false);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    setConsoleLog("Permission Not Granted", false);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    public static void setConsoleLog(final String msg, final boolean reset) {
        try {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        if (console == null) return;
                        if (reset) {
                            console.setText("");
                        }
                        String src = (String) console.getText();
                        src = src + "\n" + msg;
                        console.setText(src);
                        Log.d("RMS", msg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = MainActivity.this;
        setContentView(R.layout.activity_main);

        console = (TextView) findViewById(R.id.console);
        listView = (ListView) findViewById(R.id.listView);
        for (int i = 0 ; i < fNames.length ; i++) {
            functions.add(fNames[i]);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.aLine, functions);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String data = (String) adapterView.getItemAtPosition(position);
                setConsoleLog("Perform "+data, true);
                DataCollector.getInstance(activity).performCmd(data);
            }
        });
        // It changes background of selected item
        listView.setSelector(R.color.material_on_background_disabled);
        listView.setSelected(true);
        verifyStoragePermissions(this);

        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            powerManager.addThermalStatusListener(new PowerManager.OnThermalStatusChangedListener() {
                    @Override
                    public void onThermalStatusChanged(int status) {
                        switch (status)
                        {
                            case PowerManager.THERMAL_STATUS_NONE:
                                Log.d("Thermal", "NONE");
                                thermalStatus = "NONE";
                                break;

                            case PowerManager.THERMAL_STATUS_LIGHT:
                                Log.d("Thermal", "LIGHT");
                                thermalStatus = "LIGHT";
                                break;

                            case PowerManager.THERMAL_STATUS_MODERATE:
                                Log.d("Thermal", "MODERATE");
                                thermalStatus = "MODERATE";
                                break;

                            case PowerManager.THERMAL_STATUS_SEVERE:
                                Log.d("Thermal", "SEVERE");
                                thermalStatus = "SEVERE";
                                break;

                            case PowerManager.THERMAL_STATUS_CRITICAL:
                                Log.d("Thermal", "CRITICAL");
                                thermalStatus = "CRITICAL";
                                break;

                            case PowerManager.THERMAL_STATUS_EMERGENCY:
                                Log.d("Thermal", "EMERGENCY");
                                thermalStatus = "EMERGENCY";
                                break;

                            case PowerManager.THERMAL_STATUS_SHUTDOWN:
                                Log.d("Thermal", "SHUTDOWN");
                                thermalStatus = "SHUTDOWN";
                                break;
                        }
                    }
                }
            );
        }
    }

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setSelection(0);
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        /*
        if (event.getAction() != KeyEvent.ACTION_DOWN) return false;
        Utils.printConsole("onKey: "+keyCode, false);
        if (keyCode == 66) {
        //if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
            switch (v.getId()) {
                case R.id.device:
                    DataCollector.getInstance(this).collect(DataCollector.TYPE_DEVICE);
                    break;
                case R.id.hardware:
                    DataCollector.getInstance(this).collect(DataCollector.TYPE_HW);
                    break;
                case R.id.network:
                    DataCollector.getInstance(this).collect(DataCollector.TYPE_NETWORK);
                    break;
                case R.id.application:
                    DataCollector.getInstance(this).collect(DataCollector.TYPE_APP);
                    break;
                case R.id.software:
                    DataCollector.getInstance(this).collect(DataCollector.TYPE_SW_UPDATE);
                    break;
            }
        } else if (isNavigationKey(keyCode)) {
            keyNavigation(v, keyCode);
        }

         */
        return false;
    }

    private boolean isNavigationKey(int keyCode) {
        /*
        if (keyCode == KeyEvent.KEYCODE_DPAD_UP) return true;
        if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) return true;
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) return true;
        if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) return true;
         */
        return false;
    }
}
