package com.alticast.charter.telemetry.collector;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.InetAddresses;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.CpuUsageInfo;
import android.os.Debug;
import android.os.Environment;
import android.os.HardwarePropertiesManager;
import android.os.PowerManager;
import android.os.StatFs;
import android.os.SystemClock;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.provider.Settings;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.alticast.charter.telemetry.MainActivity;
import com.alticast.charter.telemetry.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class DataCollector {
    private static final String TAG = "DataCollector";
    public static final int TYPE_DEVICE = 0;
    public static final int TYPE_HW = 1;
    public static final int TYPE_NETWORK= 2;
    public static final int TYPE_APP = 3;
    public static final int TYPE_SW_UPDATE = 4;
    private static Context ctx;
    private static DataCollector instance;

    private DataCollector() {
    }

    private void consolePrint(String msg) {
        ((MainActivity)ctx).setConsoleLog(msg, false);
    }

    public static DataCollector getInstance(Context context) {
        ctx = context;
        if (instance == null) instance = new DataCollector();
        return instance;
    }

    public void performCmd(String cmd) {
        consolePrint("================================================");
        try {
            Thread t = new Thread(){
                public void run() {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    try {
                        if (cmd.equals("getTime")) {
                            map.put("currentTime", System.currentTimeMillis()+"");
                            map.put("upTime", SystemClock.uptimeMillis()+"");
                            map.put("timeZone", TimeZone.getDefault().getDisplayName());
                            map.put("lastBootTime", (System.currentTimeMillis()-SystemClock.elapsedRealtime())+"");
                        } else if (cmd.equals("getVersion")) {
                            map.put("androidOsVersion", Build.VERSION.SDK_INT+"");
                            map.put("firmwareVersion", Build.FINGERPRINT);
                            map.put("modelName", Build.MODEL);
                        } else if (cmd.equals("getDiagInfo")) {
                            map.put("TBD", "TBD");
                        } else if (cmd.equals("getCpuUsage")) {
                            /*
                            HardwarePropertiesManager hardwarePropertiesManager = ctx.getApplicationContext().getSystemService(HardwarePropertiesManager.class);
                            CpuUsageInfo[] cpuUsages = hardwarePropertiesManager.getCpuUsages();
                            for (int i = 0 ; i < cpuUsages.length ; i++) {
                                map.put("cpu "+i, cpuUsages[i].toString());
                            }
                             */

                            //Process p = Runtime.getRuntime().exec("cat /proc/stat");
                            /*
                            Process p = Runtime.getRuntime().exec("top -n 1");
                            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                            String line = "";
                            int i = 0;
                            while ((line = br.readLine()) != null) {
                                map.put("top "+i, line);
                                i++;
                            }
                             */
                            ///////////////////
                            // Sample data
                            ///////////////////
                            map.put("user", "30%");
                            map.put("nice", "0%");
                            map.put("system", "20%");
                            map.put("idle", "343%");
                            map.put("lowait", "0%");
                            map.put("irq", "3%");
                            map.put("softirq", "3%");

                            String[] processes = {
                                    "android.hardware.audio.service-droidlogic"
                                    ,"com.alticast.charter.telemetry"
                                    ,"com.android.keychain"
                                    ,"[kworker/2:0]"
                                    ,"[kworker/3:0-events]"
                                    ,"[kworker/1:0H-kblockd]"
                                    ,"[kworker/3:0H-kblockd]"
                                    ,"[kworker/u8:4-events_unbound]"
                                    ,"[kworker/u8:3-kverityd]"
                                    ,"[kworker/3:2-events]"
                            };
                            String[] usages = {
                                    "20.0%"
                                    ,"13.3%"
                                    ,"2.1%"
                                    ,"0.3"
                                    ,"0.0"
                                    ,"0.0"
                                    ,"0.0"
                                    ,"0.0"
                                    ,"0.0"
                                    ,"0.0"
                            };

                            Map<String, String> [] mapArr = new LinkedHashMap[10];
                            for (int i = 0 ; i < mapArr.length ; i++) {
                                mapArr[i] = new LinkedHashMap<>();
                                mapArr[i].put("processName", processes[i]);
                                mapArr[i].put("cpuUsage", usages[i]);
                            }
                            JSONArray arr = new JSONArray(mapArr);
                            //map.put("top10Processes", arr.toString().replaceAll("\"", ""));
                            map.put("top10Processes", arr.toString());
                        } else if (cmd.equals("getStorageUsage")) {
                            long internalTotalSpace
                                    = new StatFs(Environment.getDataDirectory().getPath()).getTotalBytes();
                            map.put("internal path", Environment.getDataDirectory().getPath());
                            map.put("external path", Environment.getExternalStorageDirectory().getPath());
                            long internalFreeSpace
                                    = new StatFs(Environment.getDataDirectory().getPath()).getFreeBytes();
                            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
                            long externalTotalSpace = (long) stat.getTotalBytes();
                            long externalFreeSpace = (long) stat.getFreeBytes();
                            final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
                            int digitGroups0 = (int) (Math.log10(internalTotalSpace)/Math.log10(1024));
                            int digitGroups1 = (int) (Math.log10(internalFreeSpace)/Math.log10(1024));
                            int digitGroups2 = (int) (Math.log10(externalTotalSpace)/Math.log10(1024));
                            int digitGroups3 = (int) (Math.log10(externalFreeSpace)/Math.log10(1024));
                            String size0 = new DecimalFormat("#,##0.#").format(
                                    internalTotalSpace/Math.pow(1024, digitGroups0)) + " " + units[digitGroups0];
                            String size1 = new DecimalFormat("#,##0.#").format(
                                    internalFreeSpace/Math.pow(1024, digitGroups1)) + " " + units[digitGroups1];
                            String size2 = new DecimalFormat("#,##0.#").format(
                                    externalTotalSpace/Math.pow(1024, digitGroups2)) + " " + units[digitGroups2];
                            String size3 = new DecimalFormat("#,##0.#").format(
                                    externalFreeSpace/Math.pow(1024, digitGroups3)) + " " + units[digitGroups3];
                            map.put("internalTotal", size0);
                            map.put("internalFree ", size1);
                            map.put("externalTotal", size2);
                            map.put("externalFree ", size3);
                        } else if (cmd.equals("getMemoryUsage")) {
                            ActivityManager am=(ActivityManager)ctx.getSystemService(Context.ACTIVITY_SERVICE);
                            ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
                            am.getMemoryInfo(mi);
                            final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
                            int digitGroups0 = (int) (Math.log10(mi.totalMem)/Math.log10(1024));
                            int digitGroups1 = (int) (Math.log10(mi.availMem)/Math.log10(1024));
                            String size0 = new DecimalFormat("#,##0.#").format(
                                    mi.totalMem/Math.pow(1024, digitGroups0)) + " " + units[digitGroups0];
                            String size1 = new DecimalFormat("#,##0.#").format(
                                    mi.availMem/Math.pow(1024, digitGroups1)) + " " + units[digitGroups1];
                            map.put("totalMemory", size0);
                            map.put("availableMemory", size1);
                            /*
                            ///////////////////
                            // Real data
                            ///////////////////
                            List<RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
                            for (int i = 0 ; i < runningApps.size() ; i++) {
                                RunningAppProcessInfo info = runningApps.get(i);
                                Debug.MemoryInfo dmi = am.getProcessMemoryInfo(new int[]{info.pid})[0];
                                map.put("app"+i, info.processName
                                        +" : pid="+info.pid+" : nativePss="+dmi.nativePss+"kB : otherPss="+dmi.otherPss+"kB");
                            }
                             */
                            ///////////////////
                            // Sample data
                            ///////////////////
                            String[] packageNames = {
                                    "android.hardware.audio.service-droidlogic"
                                    ,"com.alticast.charter.telemetry"
                                    ,"com.android.keychain"
                                    ,"system.all.captivaPortalLoagin"
                                    ,"com.droidlogic.appinstall"
                                    ,"com.android.permissioncontroller"
                                    ,"com.android.systemui"
                            };
                            String[] pids = {
                                    "10041"
                                    ,"10038"
                                    ,"10044"
                                    ,"10023"
                                    ,"10021"
                                    ,"10011"
                                    ,"10012"
                            };
                            String[] nativePss = {
                                    "10087kB"
                                    ,"8890kB"
                                    ,"7323kB"
                                    ,"3232kB"
                                    ,"3023kB"
                                    ,"987kB"
                                    ,"103kB"
                            };
                            String[] otherPss = {
                                    "20194kB"
                                    ,"18232kB"
                                    ,"4325kB"
                                    ,"4323kB"
                                    ,"3023kB"
                                    ,"1923kB"
                                    ,"1543kB"
                            };
                            Map<String, String> [] mapArr = new LinkedHashMap[7];
                            for (int i = 0 ; i < mapArr.length ; i++) {
                                mapArr[i] = new LinkedHashMap<>();
                                mapArr[i].put("packageName", packageNames[i]);
                                mapArr[i].put("pid", pids[i]);
                                mapArr[i].put("nativePss", nativePss[i]);
                                mapArr[i].put("otherPss", otherPss[i]);
                            }
                            JSONArray arr = new JSONArray(mapArr);
                            map.put("appProcesses", arr.toString());
                        } else if (cmd.equals("getNetworkInterface")) {
                            int i = 0;
                            InetAddress addr = null;
                            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                                NetworkInterface intf = en.nextElement();
                                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                                    InetAddress inetAddress = enumIpAddr.nextElement();
                                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                                        //map.put("hostAddr"+i, inetAddress.getHostAddress());
                                        addr = inetAddress;
                                        //TODO : do not break and collect all inetAddresses in real implementation.
                                        break;
                                    }
                                    i++;
                                }
                            }
                            if (addr != null) {
                                NetworkInterface ni = NetworkInterface.getByInetAddress(addr);
                                map.put("interfaceName", ni.getName());
                                map.put("ipAddr", addr.getHostName());
                                map.put("isMulticastAddr", addr.isMulticastAddress()+"");
                                map.put("isLocalAddr", addr.isAnyLocalAddress()+"");
                                map.put("isLoopback", addr.isLoopbackAddress()+"");
                                map.put("isUp", ni.isUp()+"");
                                map.put("isVirtual", ni.isVirtual()+"");
                                ConnectivityManager connectivityManager = (ConnectivityManager) MainActivity.activity.getSystemService(CONNECTIVITY_SERVICE);
                                for (Network network : connectivityManager.getAllNetworks()) {
                                    NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
                                    if (networkInfo.isConnected()) {
                                        LinkProperties lp = connectivityManager.getLinkProperties(network);
                                        if (lp!= null && lp.getInterfaceName().equals(ni.getName())) {
                                            map.put("dns", lp.getDnsServers().get(0).getHostAddress());
                                        }
                                    }
                                }
                            }
                            //Process p = Runtime.getRuntime().exec("ifconfig");
                            /*
                            Process p = Runtime.getRuntime().exec("netstat -r -n");
                            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                            String line = "";
                            int i = 0;
                            while ((line = br.readLine()) != null) {
                                map.put("ifconfig "+i, line);
                                i++;
                                JSONObject json = new JSONObject(map);
                                consolePrint(json.toString(4));
                            }
                             */
                        } else if (cmd.equals("getSetupValue")) {
                            map.put("ADB_ENABLED", Settings.Global.getString(ctx.getContentResolver(), Settings.Global.ADB_ENABLED));
                        } else if (cmd.equals("getProcesses")) {
                            ///////////////////
                            // Real data
                            ///////////////////
                            /*
                            ActivityManager am = (ActivityManager) MainActivity.activity.getSystemService(Context.ACTIVITY_SERVICE);
                            //TODO : it just returns this app's pid, so need to test it as a system app.
                            List<ActivityManager.RunningAppProcessInfo> pids = am.getRunningAppProcesses();
                            for (int i = 0; i < pids.size(); i++) {
                                map.put("pid"+i, pids.get(i).processName+" : "+pids.get(i).pid);
                            }
                             */
                            ///////////////////
                            // Sample data
                            ///////////////////
                            String[] pids = {
                                   "1"
                                   ,"2"
                                    ,"100"
                                    ,"102"
                                    ,"104"
                                    ,"105"
                                    ,"106"
                                    ,"324"
                                    ,"325"
                                    ,"326"
                            };
                            String[] processNames = {
                                    "credstore /data/misc/credstorex"
                                    ,"tvserver"
                                    ,"surfaceflinger"
                                    ,"keystore /data/misc/keystore"
                                    ,"media.extractor aextractor"
                                    ,"media.codec hw/android.hardware.media.omx@1.0-service"
                                    ,"gatekeeperd /data/misc/gatekeeper"
                                    ,"update_engine --logtostderr --logtofile --foreground"
                                    ,"adbd --root_seclabel=u:r:su:s0"
                                    ,"traced_probes"
                            };
                            Map<String, String> [] mapArr = new LinkedHashMap[10];
                            for (int i = 0 ; i < mapArr.length ; i++) {
                                mapArr[i] = new LinkedHashMap<>();
                                mapArr[i].put("pid", pids[i]);
                                mapArr[i].put("processName", processNames[i]);
                            }
                            JSONArray arr = new JSONArray(mapArr);
                            consolePrint(arr.toString(4));
                            // It returns here!
                            return;

                            /* "ps -ef" collects only for this app.
                            Process p = Runtime.getRuntime().exec("ps -ef");
                            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                            String line = "";
                            int i = 0;
                            while ((line = br.readLine()) != null) {
                                map.put("ps "+i, line);
                                i++;
                                JSONObject json = new JSONObject(map);
                                consolePrint(json.toString(4));
                            }
                             */
                        } else if (cmd.equals("reboot")) {
                            if (ContextCompat.checkSelfPermission(ctx,
                                    Manifest.permission.REBOOT) != PackageManager.PERMISSION_GRANTED) {
                                consolePrint("REBOOT permission is not granted");

                                // Permission is not granted
                                // Should we show an explanation?
                                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.activity,
                                        Manifest.permission.REBOOT)) {
                                    // Show an explanation to the user *asynchronously* -- don't block
                                    // this thread waiting for the user's response! After the user
                                    // sees the explanation, try again to request the permission.
                                } else {
                                    consolePrint("Request Permission");
                                    // No explanation needed; request the permission
                                    ActivityCompat.requestPermissions(MainActivity.activity,
                                            new String[]{Manifest.permission.REBOOT},
                                            101);
                                    /*
                                    MainActivity.activity.startActivity(
                                            new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                                     */

                                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                    // app-defined int constant. The callback method gets the
                                    // result of the request.
                                }
                            } else {
                                // Permission has already been granted
                                consolePrint("REBOOT permission is granted");
                                PowerManager pm=(PowerManager)ctx.getSystemService(Context.POWER_SERVICE);
                                pm.reboot(null);
                                return;
                            }
                        } else if (cmd.equals("factoryReset")) {
                        } else if (cmd.equals("showDiagScreen")) {
                        } else if (cmd.equals("killProcess")) {
                            android.os.Process.killProcess(android.os.Process.myPid());
                        } else if (cmd.equals("getThermalStatus")) {
                            map.put("thermalStatus", MainActivity.thermalStatus);
                        } else if (cmd.equals("uploadFile")) {
                            // Upload is done by JC. I just verify file-read located in various folder.
                            // 1. file made by app : it works
                            String path = "/sdcard/logcat/aaa.txt";

                            // 2. files of the other apps : File.exists() returns false
                            // if remove exists(), then android.system.ErrnoException: open failed: EACCES (Permission denied)
                            //String path = "/sdcard/Android/data/com.alticast.atsc3/files/service_list";

                            // 3. system file : File.exists() returns true
                            // if remove exist(), then java.io.FileNotFoundException: /proc/stat: open failed: EACCES (Permission denied)
                            //String path = "/proc/stat";
                            File file = new File(path);
                            if (file.exists()) {
                                map.put("file path ", path);
                                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                                int i = 0;
                                String line = br.readLine();
                                while(line != null) {
                                    map.put("fileRead "+i, line);
                                    i++;
                                    line = br.readLine();
                                }
                                br.close();
                            }
                        } else if (cmd.equals("deleteFile")) {
                            // 1. file made by app : it works
                            String path = "/sdcard/logcat/aaa.txt";
                            // 2. files of the other apps : File.exists() returns false And delete() return false
                            //String path = "/sdcard/Android/data/com.alticast.atsc3/files/service_list";
                            // 3. system file : File.exists() returns true And delete() returns false
                            //String path = "/proc/stat";
                            File file = new File(path);
                            //if (file.exists()) {
                                boolean result = file.delete();
                                map.put("file path ", path);
                                map.put("deleted ", result+"");
                            //}
                        } else if (cmd.equals("setLogPolicy")) {
                        } else if (cmd.equals("getLogPolicy")) {
                        } else if (cmd.equals("uploadLog")) {
                            // Unloading will be done by JC. I just try to cache logcat logs in a file
                            LogcatManager.getInstance().activate("aaa.txt");
                            map.put("logcat ", "start to capture log");
                            /*
                            Runtime.getRuntime().exec("logcat -c");
                            //LISTEN TO NEW LOGS
                            Process p=Runtime.getRuntime().exec("logcat v main");
                            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                            String line = "";
                            int i = 0;
                            while ((line = br.readLine()) != null) {
                                map.put("logcat "+i, line);
                                i++;
                                JSONObject json = new JSONObject(map);
                                consolePrint(json.toString(4));
                            }
                            Log.d("logcat_test", "logcat finished");
                            return;
                             */
                        } else if (cmd.equals("getRunningAppInfo")) {
                            ///////////////////
                            // Sample data : just 10 installed app
                            ///////////////////
                            final PackageManager pm = MainActivity.activity.getPackageManager();
//get a list of installed apps.
                            List<ApplicationInfo> apps =
                                    pm.getInstalledApplications(PackageManager.GET_META_DATA);
                            Map<String, String> [] mapArr = new LinkedHashMap[10];
                            for (int i = 0 ; i < mapArr.length ; i++) {
                                mapArr[i] = new LinkedHashMap<>();
                                mapArr[i].put("name", apps.get(i).name);
                                mapArr[i].put("package", apps.get(i).packageName);
                                PackageInfo pi = pm.getPackageInfo(apps.get(i).packageName, 0);
                                mapArr[i].put("version", pi.versionName);
                                mapArr[i].put("srcDir", apps.get(i).sourceDir);
                                mapArr[i].put("minSdk", apps.get(i).minSdkVersion+"");
                                mapArr[i].put("targetSdk", apps.get(i).targetSdkVersion+"");
                                mapArr[i].put("uid", apps.get(i).uid+"");
                                mapArr[i].put("dataDir", apps.get(i).dataDir);
                            }
                            JSONArray arr = new JSONArray(mapArr);
                            consolePrint(arr.toString(4));
                            // It returns here!
                            return;

                        } else if (cmd.equals("getInstalledAppInfo")) {
                            /* dumpsys command is not permitted. I need to test it as a system app.
                            Process p = Runtime.getRuntime().exec("dumpsys package com.alticast.charter.telemetry");
                            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                            String line = "";
                            int i = 0;
                            while ((line = br.readLine()) != null) {
                                map.put("dumpsys "+i, line);
                                i++;
                                JSONObject json = new JSONObject(map);
                                consolePrint(json.toString(4));
                            }
                             */
                            final PackageManager pm = MainActivity.activity.getPackageManager();
//get a list of installed apps.
                            List<ApplicationInfo> apps =
                                    pm.getInstalledApplications(PackageManager.GET_META_DATA);
                            Map<String, String> [] mapArr = new LinkedHashMap[apps.size()];
                            for (int i = 0 ; i < mapArr.length ; i++) {
                                mapArr[i] = new LinkedHashMap<>();
                                mapArr[i].put("name", apps.get(i).name);
                                mapArr[i].put("package", apps.get(i).packageName);
                                PackageInfo pi = pm.getPackageInfo(apps.get(i).packageName, 0);
                                mapArr[i].put("version", pi.versionName);
                                mapArr[i].put("srcDir", apps.get(i).sourceDir);
                                mapArr[i].put("minSdk", apps.get(i).minSdkVersion+"");
                                mapArr[i].put("targetSdk", apps.get(i).targetSdkVersion+"");
                                mapArr[i].put("uid", apps.get(i).uid+"");
                                mapArr[i].put("dataDir", apps.get(i).dataDir);
                            }
                            JSONArray arr = new JSONArray(mapArr);
                            consolePrint(arr.toString(4));
                            // It returns here!
                            return;
                        } else if (cmd.equals("runApp")) {
                            Intent intent = new Intent();
                            //intent.setClassName("com.droidlogic.tvinput", "com.alticast.charter.telemetry");
                            //intent.setPackage("com.alticast.atsc3");
                            //MainActivity.activity.startActivity(intent);

                            Intent i = ctx.getPackageManager().getLaunchIntentForPackage("com.alticast.atsc3");
                            ctx.startActivity(i);
                        }
                        JSONObject json = new JSONObject(map);
                        consolePrint(json.toString(4));
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            map.put("Exception", e.toString());
                            JSONObject json = new JSONObject(map);
                            consolePrint(json.toString(4));
                        } catch (Exception ex) {
                        }
                    }
                }
            };
            t.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
