package com.alticast.charter.telemetry.collector;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class LogcatManager extends Thread {
    private static final String TAG = "LogcatManager";
    private static LogcatManager instance;
    private boolean activated = false;
    private String lastFileName = "";
    private File root;
    private BufferedReader logInput;
    private BufferedWriter logOutput;
    public static LogcatManager getInstance() {
        if (instance == null) {
            instance = new LogcatManager();
            instance.start();
        }
        return instance;
    }

    private LogcatManager() {
        try {
            root = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/logcat");
            if (!root.exists()) {
                if (!root.mkdir()) {
                    Log.d(TAG, "mkdir() failed");
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void activate(String fileName) {
        if (fileName == null) return;
        try {
            if (!fileName.equals(lastFileName)) {
                // stop caching
                deactivate();

                if (logInput != null) {
                    logInput.close();
                }
                if (logOutput != null) {
                    logOutput.close();
                }
                File newLogFile = new File(root.getAbsolutePath()+"/"+fileName);
                logOutput = new BufferedWriter(new FileWriter(newLogFile));
                Runtime.getRuntime().exec("logcat -c");
                Process p = Runtime.getRuntime().exec("logcat v main");
                logInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            }
            activated = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deactivate() {
        activated = false;
    }

    public void run() {
        Log.d(TAG, "start thread");
        while (true) {
            try {
                if (activated && logInput!= null && logOutput != null) {
                    String line = logInput.readLine();
                    if (line != null) {
                        logOutput.write(line);
                        logOutput.newLine();
                        logOutput.flush();
                    }
                }
                sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
